"use strict";

import { createServer } from "node:http";
import { dirname, extname } from "node:path";
import { fileURLToPath } from "node:url";
import { networkInterfaces } from "node:os";
import { readFile, watchFile } from "node:fs";

import { Server } from "socket.io";

import { Admin, rawConfig, ServerConfig } from "./modules/serverlib.js";
import { CliPrettier } from "./modules/common.js";

// required with esm style
const __dirname = dirname(fileURLToPath(import.meta.url));

// check configuration
// NOTE priority: environment variable -> package.json -> fallback to default
// check remote admin page allowed
if (process.env.ADMIN_PAGE) {
  switch (process.env.ADMIN_PAGE) {
    case "true":
      Admin.allowHttpControl = true;
      break;
    default:
      Admin.allowHttpControl = false;
      break;
  }
} else {
  Admin.allowHttpControl = process.env.npm_package_config_allowHttpControl;
}

// check remote admin page allowed
if (process.env.ADMIN_REMOTE) {
  switch (process.env.ADMIN_REMOTE) {
    case "true":
      Admin.allowGlobalControl = true;
      break;
    default:
      Admin.allowGlobalControl = false;
      break;
  }
} else {
  Admin.allowGlobalControl = process.env.npm_package_config_allowGlobalControl;
}

// welcome message here
console.log(
  "*** このプログラムは日本大学歯学部の有志によって作成されました。\n" +
    "*** ブラウザから以下のアドレスにアクセスして下さい。\n" +
    "*** (終了：Ctrl + c)\n",
);

// init appConf
const appConf = new ServerConfig();
appConf.loadConfig();

/**
 * detect configuration update, then emit val
 */
watchFile(rawConfig.filePath, () => {
  CliPrettier.server("configuration file updated, reloading");
  let updatedDate;
  try {
    appConf.loadConfig(rawConfig.content);
    updatedDate = appConf.startDate;
    io.sockets.emit("startDate", updatedDate);
  } catch (e) {
    CliPrettier.error(
      "appconfig.jsonの読み込みに失敗しました。変更前の値で続行します。",
      `${e}`,
    );
  } finally {
    io.sockets.emit("initCounter", 0);
  }
});

// list available NICs
const availableIface = networkInterfaces();
Object.keys(availableIface).forEach((key) => {
  console.log(`\n@${key}`);
  const ipv4faces = availableIface[key].filter(
    (addr) => addr["family"] === "IPv4",
  );
  ipv4faces.forEach((obj) =>
    console.log(
      CliPrettier.colors.fgMagenta +
        `http://${obj.address}:${appConf.exposedPort}` +
        CliPrettier.colors.reset,
    )
  );
});
console.log(
  CliPrettier.colors.bgGreen +
    CliPrettier.colors.fgBlack +
    "\n*** server log ***" +
    CliPrettier.colors.reset,
);

/**
 * request handler
 */
const mimeTypes = {
  ".html": "text/html",
  ".js": "text/javascript",
  ".mp3": "audio/mpeg",
  ".ico": "image/x-icon",
  ".css": "text/css",
};

/** check given req from loopback addr
 * returns true if request is from loopback address
 */
function isReqFromLoopbackAddr(req) {
  if (req.socket.remoteFamily === "IPv4") {
    return req.socket.remoteAddress === "127.0.0.1" ? true : false;
  } else if (req.socket.remoteFamily === "IPv6") {
    return req.socket.remoteAddress.split(":").slice(-1)[0] === "127.0.0.1" ||
        req.socket.remoteAddress === "::1"
      ? true
      : false;
  }
}

const handler = (req, res) => {
  const url = req.url;
  const emphasis = { start: "", end: CliPrettier.colors.reset };
  if (url == "/admin") {
    emphasis.start = CliPrettier.colors.bgMagenta;
  } else if (String(url).match(/mp3$/i)) {
    emphasis.start = CliPrettier.colors.bgCyan + CliPrettier.colors.fgBlack;
  }
  CliPrettier.get(
    `${req.connection.remoteAddress}`,
    `${emphasis.start}${url}${emphasis.end}`,
  );

  const pathPrefix = `${__dirname}/public`;
  const fileExt = String(extname(url)).toLowerCase();
  const contentType = mimeTypes[fileExt] || "text/html";

  // handle html page request
  function returnHtmlFile(filename) {
    readFile(`${pathPrefix}/${filename}`, (err, content) => {
      if (err) {
        res.writeHead(500);
        res.end(`Error loading ${filename}`);
      }
      res.writeHead(200, { "Content-Type": contentType });
      res.end(content, "utf-8");
    });
  }

  // process request
  switch (url) {
    case "/":
      returnHtmlFile("index.html");
      break;

    case "/a":
      returnHtmlFile("index_a.html");
      break;

    case "/admin":
      if (
        Admin.allowHttpControl &&
        (Admin.allowGlobalControl ? true : isReqFromLoopbackAddr(req))
      ) {
        returnHtmlFile("admin.html");
        break;
      } else {
        res.writeHead(500, { "Content-Type": "text/plain" });
        res.end("admin page not enabled", "utf-8");
        CliPrettier.error("admin page access denied");
        break;
      }
    default:
      readFile(pathPrefix + url, (err, content) => {
        if (err) {
          if (err.code === "ENOENT") {
            res.writeHead(404, { "Content-Type": "text/plain" });
            res.end(`file doesn't exist ${url}`, "utf-8");
          } else {
            res.writeHead(500, { "Content-Type": "text/plain" });
            res.end(`invalid request ${url}`, "utf-8");
          }
        } else {
          res.writeHead(200, { "Content-Type": contentType });
          res.end(content, "utf-8");
        }
      });
      break;
  }
};

/**
 * launch server
 */
const app = createServer(handler).listen(appConf.exposedPort);
const io = new Server();
io.attach(app);
Admin.socketServer = io;

/**
 * socket connection to clients
 */
io.on("connection", (socket) => {
  socket.emit("startDate", appConf.startDate);

  /**
   * sync control from admin page
   */
  if (Admin.allowHttpControl) {
    socket.on("serverControl", (data) => {
      const currentTime = new Date().toLocaleString("ja-JP");
      try {
        // process server start/stop
        Admin.parseCommand(appConf, data, currentTime, socket);
      } catch (e) {
        CliPrettier.error("ineffective command received", `${e}`);
        socket.emit("serverControl", `${e} @ ${currentTime}`);
      }
    });
  }
});

Admin.startEmitTime();
