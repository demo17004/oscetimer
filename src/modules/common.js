"use strict";

/** prettier log
 * make console log a bit prettier
 */
export class CliPrettier {
  static colors = {
    fgBlack: "\x1b[30m",
    fgRed: "\x1b[31m",
    fgGreen: "\x1b[32m",
    fgBlue: "\x1b[34m",
    fgMagenta: "\x1b[35m",
    fgCyan: "\x1b[36m",
    bgRed: "\x1b[41m",
    bgGreen: "\x1b[42m",
    bgBlue: "\x1b[44m",
    bgMagenta: "\x1b[45m",
    bgCyan: "\x1b[46m",
    bgWhite: "\x1b[47m",
    reset: "\x1b[0m",
  };
  static debugMode = false;

  static error(msg, detail = "") {
    console.error(
      CliPrettier.colors.bgRed +
        "  ERROR " +
        CliPrettier.colors.reset +
        " " +
        CliPrettier.colors.fgRed +
        " - " +
        new Date().toLocaleString("ja-JP") +
        CliPrettier.colors.fgRed +
        " - " +
        msg +
        CliPrettier.colors.fgRed +
        " - " +
        detail +
        CliPrettier.colors.reset,
    );
  }
  static remote(msg) {
    console.log(
      CliPrettier.colors.bgGreen +
        CliPrettier.colors.fgBlack +
        " REMOTE " +
        CliPrettier.colors.reset +
        " " +
        " - " +
        new Date().toLocaleString("ja-JP") +
        " - " +
        msg,
    );
  }
  static get(from, target = "") {
    console.log(
      CliPrettier.colors.bgWhite +
        CliPrettier.colors.fgBlack +
        "    GET " +
        CliPrettier.colors.reset +
        " " +
        " - " +
        new Date().toLocaleString("ja-JP") +
        " - " +
        from +
        " -> " +
        target,
    );
  }
  static server(msg) {
    console.log(
      CliPrettier.colors.bgCyan +
        CliPrettier.colors.fgBlack +
        " SERVER " +
        CliPrettier.colors.reset +
        " " +
        " - " +
        new Date().toLocaleString("ja-JP") +
        " - " +
        msg,
    );
  }

  static debug(msg) {
    if (CliPrettier.debugMode) {
      console.log(
        CliPrettier.colors.bgBlue +
          CliPrettier.colors.fgBlack +
          " DEBUG " +
          CliPrettier.colors.reset +
          "  " +
          " - " +
          new Date().toLocaleString("ja-JP") +
          " - " +
          msg,
      );
    }
  }
}
