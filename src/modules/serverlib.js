"use strict";
import { readFileSync, writeFileSync } from "node:fs";

import { CliPrettier } from "./common.js";

function convertDateToStr(dateobj) {
  // convert Dateobj to YYYY-mm-dd HH:MM:SS
  return (
    `${dateobj.getFullYear()}` +
    "-" +
    `${dateobj.getMonth() + 1}` +
    "-" +
    `${dateobj.getDate()}` +
    " " +
    `${dateobj.getHours()}`.padStart(2, "0") +
    ":" +
    `${dateobj.getMinutes()}`.padStart(2, "0") +
    ":" +
    `${dateobj.getSeconds()}`.padStart(2, "0")
  );
}

function convertDatePropToDateObj(startDate) {
  return new Date(
    `${startDate.year}-` +
      `${startDate.month}`.padStart(2, "0") +
      "-" +
      `${startDate.date}`.padStart(2, "0") +
      "T" +
      `${startDate.hour}`.padStart(2, "0") +
      ":" +
      `${startDate.minute}`.padStart(2, "0"), // use server local timezone
  );
}

let timerID = null; // init timerID

export const rawConfig = {
  filePath: "appconfig.json",
  get content() {
    let readObj;
    try {
      CliPrettier.server("loading appconfig.json...");
      readObj = JSON.parse(readFileSync(this.filePath, "utf-8"));
    } catch (e) {
      CliPrettier.error("loading failed, use default", `${e}`);
      const currentDate = new Date(Date.now());
      let startYear = currentDate.getFullYear();
      if (currentDate.getMonth() != 0) startYear += 1;
      readObj = {
        exposedPort: 3000,
        startDate: {
          year: startYear,
          month: 1,
          date: 18,
          hour: 9,
          minute: 0,
        },
      };
      // write generated default
      CliPrettier.server("generate appconfig.json with default");
      writeFileSync(
        this.filePath,
        JSON.stringify(readObj, null, "    "),
        (err) => {
          if (err) {
            throw err;
          }
        },
      );
    }
    return readObj;
  },
};

/** Server configuration class
 * Store server configuration parameters
 */
export class ServerConfig {
  #defaultConfObj;
  #rawConfObj;
  static isPropertyCorrect(conf) {
    if (
      !(
        "exposedPort" in conf &&
        "year" in conf.startDate &&
        "month" in conf.startDate &&
        "date" in conf.startDate &&
        "hour" in conf.startDate &&
        "minute" in conf.startDate
      )
    ) {
      throw "invalid appconf format";
    }
  }
  static isStartDateCorrect(startDate) {
    const newDate = convertDatePropToDateObj(startDate);
    if (Number.isNaN(newDate.getDate())) {
      throw "invalid date format";
    }
  }
  static isPortCorrect(conf) {
    if (typeof conf.exposedPort !== "number") {
      throw `${conf.exposedPort} is not Number.`;
    } else if (conf.exposedPort < 0 || 65535 < conf.exposedPort) {
      throw `${conf.exposedPort} is out of range ${0} - ${65535}.`;
    }
  } // NOTE エラー検出する関数、これでいい？

  constructor() {
    const serverInitDate = new Date(Date.now());
    this.#defaultConfObj = {
      // init data
      exposedPort: 3000,
      startDate: {
        year: `${serverInitDate.getFullYear()}`,
        month: `${serverInitDate.getMonth() + 1}`,
        date: 1,
        hour: 9,
        minute: 0,
      },
    };
  }

  loadConfig(configObj = rawConfig.content) {
    try {
      // validate format
      CliPrettier.debug("cheking format of: " + `${configObj}`);
      ServerConfig.isPropertyCorrect(configObj);
      ServerConfig.isPortCorrect(configObj);
      ServerConfig.isStartDateCorrect(configObj.startDate);

      this.#rawConfObj = configObj;
      this.#defaultConfObj = configObj;
    } catch (e) {
      CliPrettier.error(
        "appconf.json format is not correct, load previous (or default)",
        `${e}`,
      );
      this.#rawConfObj = this.#defaultConfObj;
      CliPrettier.debug(`#rawConfObj: ${this.#rawConfObj}`);
    } finally {
      CliPrettier.server(
        CliPrettier.colors.fgMagenta +
          `start date info updated to [${this.startDate.toLocaleString(
            "ja-JP",
          )}]` +
          CliPrettier.colors.reset,
      );
    }
  }

  get startDate() {
    return convertDatePropToDateObj(this.#rawConfObj.startDate);
  }

  set startDate(startDate) {
    // validate startDate format
    try {
      ServerConfig.isStartDateCorrect(startDate);
    } catch (e) {
      CliPrettier.error("given configuration is not valid", `${e}`);
    } finally {
      CliPrettier.server("startDate updated");
    }

    this.#rawConfObj.startDate = startDate;
  }

  get exposedPort() {
    return Number(this.#rawConfObj.exposedPort);
  }

  get allConfig() {
    return this.#rawConfObj;
  }
}

/** Admin page related class
 * parse admin page command
 */
export class Admin {
  static socketServer = null;
  static allowHttpControl = false;
  static allowGlobalControl = false;

  static startEmitTime() {
    if (timerID == null) {
      timerID = setInterval(() => {
        const stnow = Date.now();
        Admin.socketServer.sockets.emit("stnow", stnow);
      }, 250);
    } else {
      throw "timer already running";
    }
  }

  static stopEmitTime() {
    if (timerID != null) {
      clearInterval(timerID);
      timerID = null;
    } else {
      throw "timer not running";
    }
  }

  static parseControl(content, currentDate, socket) {
    let msg;
    switch (content) {
      case "start":
        msg = "Received start signal. Emitting time start.";
        Admin.startEmitTime();
        break;
      case "stop":
        msg = "Received stop signal. Emitting time stop.";
        Admin.stopEmitTime();
        break;
      default:
        throw "invalid control command";
    }
    CliPrettier.remote(msg);
    socket.emit("serverControl", msg + ` @ ${currentDate}`);
  }

  static parseCommand(appConf, data, currentDate, socket) {
    let msg;
    switch (data.ctype) {
      case "control":
        Admin.parseControl(data.content, currentDate, socket);
        break;
      case "setDate":
        ServerConfig.isStartDateCorrect(data.content);
        appConf.startDate = data.content;
        msg = `startDate update request to ${convertDateToStr(
          appConf.startDate,
        )}`;
        CliPrettier.remote(msg);
        // NOTE update takes effect after configuration file update
        // save updated date to file
        writeFileSync(
          rawConfig.filePath,
          JSON.stringify(appConf.allConfig, null, "    "),
          (err) => {
            if (err) {
              throw err;
            }
          },
        );
        socket.emit("serverControl", msg + ` @ ${currentDate}`);
        break;
      default:
        throw "invalid command type";
    }
  }
}
