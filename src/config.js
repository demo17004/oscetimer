"use strict";
import { writeFileSync } from "node:fs";
import { questionInt } from "readline-sync";

function validateConfig(conf) {
  // validation func
  function isNumberInRange(val, min, max) {
    if (typeof val !== "number") {
      throw `${val} is not Number.`;
    } else if (val < min || max < val) {
      throw `${val} is out of range ${min} - ${max}.`;
    }
  }
  isNumberInRange(conf.exposedPort, 0, 65535);
  isNumberInRange(conf.startDate.date, 1, 31);
  isNumberInRange(conf.startDate.hour, 0, 23);
  isNumberInRange(conf.startDate.minute, 0, 59);
}

const currentDate = new Date(Date.now());
function genConf() {
  console.log("generate config file\n");
  let confobj = new Object();
  confobj = {
    exposedPort: 3000,
    startDate: {
      year: currentDate.getFullYear(),
      month: currentDate.getMonth() + 1,
      date: 1, //開始日を入力
      hour: 9, //開始時間を入力（２４時制）
      minute: 0, //開始分を入力
    },
  };

  confobj.exposedPort = questionInt(
    "Enter exposed port number. (press enter to use default: 3000)\n",
    { defaultInput: "3000" }, // value range 0-65535
  );
  confobj.startDate.startDate = questionInt(
    "Enter start day (1-31). Press enter to use default: 1\n",
    { defaultInput: "1" }, // value range 1-31
  );
  confobj.startDate.startHour = questionInt(
    "Enter start hour (0-23). Press enter to use default: 9\n",
    { defaultInput: "9" }, // value range 0-23
  );
  confobj.startDate.startMinute = questionInt(
    "Enter start minute (0-59). Press enter to use default: 0\n",
    { defaultInput: "0" }, // value range 0-59
  );
  return confobj;
}

try {
  const appconf = genConf();
  validateConfig(appconf);
  console.log(
    "\n----------------------------------------\n" +
      `開始: ${appconf.startDate.year}年 \
   ${appconf.startDate.month}月 \
   ${appconf.startDate.date}日 \
   ${appconf.startDate.hour}時 ` +
      `${appconf.startDate.minute}分`.padStart(3, "0") +
      `\nポート番号 ${appconf.exposedPort}` +
      "\n----------------------------------------\n",
  );

  writeFileSync(
    "appconfig.json",
    JSON.stringify(appconf, null, "    "),
    (err) => {
      if (err) throw err;
    },
  );
  console.log("appconfig.json generated.");
} catch (e) {
  console.error(`Error: ${e}`);
  console.error("正しい値を入力してやり直してください。");
}
