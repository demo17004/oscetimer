"use strict";

/** timer utility
 * utility funtions
 */
class TimerUtil {
  // helper functions
  static mins2millSec(minTime = 0) {
    return minTime * 60_000;
  }
  static secs2millSec(secTime = 0) {
    return secTime * 1_000;
  }
  static millSec2mins(millSec = 0) {
    return Math.floor(Math.ceil(Math.abs(millSec) / 1_000) / 60);
  }
  static millSec2secs(millSec = 0) {
    return Math.ceil(Math.ceil(Math.abs(millSec) / 1_000) % 60);
  }
  static millSec2MS(millSec = 0) {
    return {
      min: TimerUtil.millSec2mins(millSec),
      sec: TimerUtil.millSec2secs(millSec),
    };
  }
}

/** clock time object
 * clocl time object stores millsec, convert to min, sec or object
 */
class ClockTime {
  constructor(msec = 0) {
    this._msec = msec;
  }
  static createFromMinSec(minSec = { min: 0, sec: 0 }, negative = false) {
    let minSign;
    negative ? (minSign = -1) : (minSign = 1);
    return new ClockTime(
      (TimerUtil.mins2millSec(Math.abs(minSec.min)) +
        TimerUtil.mins2millSec(Math.abs(minSec.sec))) *
        minSign,
    );
  }
  get millSec() {
    return this._msec;
  }
  get min() {
    return TimerUtil.millSec2mins(this.millSec);
  }
  get sec() {
    return TimerUtil.millSec2secs(this.millSec);
  }
  get minSec() {
    return TimerUtil.millSec2MS(this.millSec);
  }
  add(ctime = new ClockTime()) {
    return new ClockTime(this.millSec + ctime.millSec);
  }
  subtract(ctime = new ClockTime()) {
    return new ClockTime(this.millSec - ctime.millSec);
  }
  eq(ctime = new ClockTime()) {
    // round a fraction under 1 sec
    return this.min === ctime.min && this.sec === ctime.sec;
  }
  eqStrict(ctime = new ClockTime()) {
    return this.millSec === ctime.millSec;
  }
  isLargerThan(ctime = new ClockTime()) {
    return this.millSec > ctime.millSec;
  }
  isSmallerThan(ctime = new ClockTime()) {
    return this.millSec < ctime.millSec;
  }
  isExactOnMinute(pMinutes = 0) {
    return this.min === pMinutes && this.sec === 0;
  }
}

/** phaase list in one round
 * each phase has name and length (ClockTime obj)
 */
export class PhaseList {
  constructor(
    phases = [
      {
        name: "moving",
        length: ClockTime.createFromMinSec({ min: 1, sec: 0 }),
      },
      {
        name: "assignmentRead",
        length: ClockTime.createFromMinSec({ min: 1, sec: 0 }),
      },
      {
        name: "assignmentRun",
        length: ClockTime.createFromMinSec({ min: 5, sec: 0 }),
      },
    ],
  ) {
    // default phase structure
    this.elist = phases;
    this.totalLength = new ClockTime();

    this._initPhase();
    this._initTotalLength();
  }

  // init length of single round
  _initTotalLength() {
    let tlen = new ClockTime();
    this.elist.forEach((phase) => {
      tlen = tlen.add(phase.length);
    });
    this.totalLength = tlen;
  }
  // init PhaseList props
  _initPhase() {
    let phaseHead = new ClockTime();
    this.elist.forEach((e) => {
      this[e.name] = {
        start: phaseHead,
        end: phaseHead.add(e.length),
        len: e.length,
      };
      phaseHead = phaseHead.add(e.length);
    });
  }
  // update current state via elapsed time
  updateCurrentState(timeElapsed = new ClockTime()) {
    if (timeElapsed.millSec < 0) {
      // not yet started
      return {
        rounds: 0,
        lapTime: 0,
        phase: {
          name: "beforeEvent",
          elapsed: timeElapsed,
          remain: timeElapsed, // remain to startdate
        },
      };
    } else {
      // after event started
      const lapTime = new ClockTime(
        timeElapsed.millSec % this.totalLength.millSec,
      );
      const roundTimes =
        Math.floor(timeElapsed.millSec / this.totalLength.millSec) + 1;
      let retState;
      let phaseStartAt = new ClockTime();
      this.elist.forEach((e) => {
        if (
          this[e.name].start.millSec <= lapTime.millSec &&
          lapTime.millSec < this[e.name].end.millSec
        ) {
          const phaseElapsed = lapTime.subtract(phaseStartAt);
          retState = {
            rounds: roundTimes,
            lapTime: lapTime,
            phase: {
              name: e.name,
              elapsed: phaseElapsed,
              remain: this[e.name].len.subtract(phaseElapsed),
            },
          };
        }
        phaseStartAt = phaseStartAt.add(this[e.name].len);
      });
      return retState;
    }
  }
}

/** local clock class
 * calculate elapsed time from received server date obj
 * holds what state it is in
 */
export class LocalClock {
  constructor(phases = new PhaseList()) {
    this.phases = phases;
    this._currentDateTime = new Date();
    this._eventStartDateTime = new Date();
    this._totalElapsed = new ClockTime();
    this.currentState = {
      rounds: 0,
      lapTime: 0,
      phase: {
        name: "beforeEvent",
        elapsed: new ClockTime(), // empty clocktime obj
        remain: new ClockTime(), // empty clocktime obj
      },
    };
  }
  set eventStartDateTime(startDateObj) {
    this._eventStartDateTime = new Date(startDateObj); // received as string
  }
  get eventStartDateTime() {
    return this._eventStartDateTime;
  }
  set currentDateTime(remoteClock) {
    // NOTE calculate and store timeDiffCurrentStart, roundTimes, lapTime on receive remoteClock
    this._currentDateTime = new Date(remoteClock.valueOf());
    this._totalElapsed = new ClockTime(
      this._currentDateTime.valueOf() - this._eventStartDateTime.valueOf(),
    );
    this.currentState = this.phases.updateCurrentState(this._totalElapsed);
  }
  get currentDateTime() {
    return this._currentDateTime;
  }
  get totalElapsed() {
    return this._totalElapsed;
  }
  get roundTimes() {
    return this.currentState.rounds;
  }
  get lapTime() {
    return this.currentState.lapTime;
  }
  get phaseRemain() {
    return this.currentState.phase.remain;
  }
  get phaseElapsed() {
    return this.currentState.phase.elapsed;
  }
  get isEventStarted() {
    return this._totalElapsed.millSec >= 0;
  }
  get timeNextPhaseStarts() {
    if (this.phase.name != "beforeEvent") {
      return new Date(
        this.currentDateTime.valueOf() + this.currentState.phase.remain.millSec,
      );
    } else {
      return this.eventStartDateTime;
    }
  }
  get phase() {
    return this.currentState.phase;
  }
}

/** page format data
 * store page color, font color and message
 */
export class PageFormat {
  constructor(pMessage = "", bgcolor = "", color = "", pTillMsg = "") {
    this.phaseMessage = pMessage;
    this.bgcolor = bgcolor;
    this.color = color;
    this.phaseTillMsg = pTillMsg;
  }
}

const oscePageFormats = {
  beforeOSCE: new PageFormat("OSCE開始まで", "gray", "blue", "開始"),
  beforeOSCE10min: new PageFormat("OSCE開始まで", "pink", "red", "開始"),
  moving: new PageFormat("移動 1分間", "skyblue", "yellow", "終了"),
  assignmentRead: new PageFormat("課題を読む 1分間", "green", "yellow", "終了"),
  assignmentRunning: new PageFormat("実技 5分間", "#3366CC", "yellow", "終了"),
};

/** draw timer page content
 * update page content
 */
class TimerPageDrawer {
  constructor() {
    this._phaseFormat = new PageFormat(); // empty format as default
    this._bdStyle = document.body.style;
    // bind html elements
    this._phaseTillMsg = document.getElementById("phaseTillMsg");
    this._startDateFromServer = document.getElementById("startDateFromServer");
    this._currentTime = {
      hour: document.getElementById("currentTimeHour"),
      min: document.getElementById("currentTimeMinute"),
      sec: document.getElementById("currentTimeSecond"),
    };
    this._phaseTill = {
      hour: document.getElementById("phaseTillHour"),
      min: document.getElementById("phaseTillMinute"),
      sec: document.getElementById("phaseTillSecond"),
    };
    this._roundNum = document.getElementById("roundNum");
    this._phaseMessage = document.getElementById("phaseMessage");
    this._phaseRemainTime = document.getElementById("phaseRemainTime");
    this._displayedStartDate = "startDate";
    // console.log("instance of TimerPageDrawer created");
  }
  static dateToElem(dateTimeObj = new Date()) {
    return {
      hour: `${dateTimeObj.getHours()}`.padStart(1, "0"),
      min: `${dateTimeObj.getMinutes()}`.padStart(2, "0"),
      sec: `${dateTimeObj.getSeconds()}`.padStart(2, "0"),
    };
  }
  static updateDomTxt(domObj, newText) {
    if (domObj.textContent != newText) domObj.textContent = newText;
  }
  get phaseFormat() {
    return this._phaseFormat;
  }
  refreshPhaseFormat(cInfo) {
    // refresh phase format and datetime if modified
    if (this._phaseFormat != cInfo.phaseFormat) {
      this._phaseFormat = cInfo.phaseFormat;
      this._bdStyle.backgroundColor = cInfo.phaseFormat.bgcolor;
      this._bdStyle.color = cInfo.phaseFormat.color;
      this._phaseTillMsg.textContent = cInfo.phaseFormat.phaseTillMsg;
    }
  }
  refreshStartDate(cInfo) {
    TimerPageDrawer.updateDomTxt(
      this._startDateFromServer,
      `${cInfo.clock.eventStartDateTime.getDate()}日 ` +
        `${cInfo.clock.eventStartDateTime.getHours()}:` +
        `${cInfo.clock.eventStartDateTime.getMinutes()}`.padStart(2, "0"),
    );
  }
  refreshCurrentTime(cInfo) {
    const newCurrentTime = TimerPageDrawer.dateToElem(
      cInfo.clock.currentDateTime,
    );
    for (const [key, val] of Object.entries(newCurrentTime)) {
      TimerPageDrawer.updateDomTxt(this._currentTime[key], val);
    }
  }
  refreshNewPhaseTill(cInfo) {
    const newPhaseTill = TimerPageDrawer.dateToElem(
      cInfo.clock.timeNextPhaseStarts,
    );
    for (const [key, val] of Object.entries(newPhaseTill)) {
      TimerPageDrawer.updateDomTxt(this._phaseTill[key], val);
    }
  }
  refreshHeader(cInfo) {
    TimerPageDrawer.updateDomTxt(
      this._roundNum,
      cInfo.clock.isEventStarted
        ? `${cInfo.clock.roundTimes}`.padStart(2, "0")
        : "",
    );
    TimerPageDrawer.updateDomTxt(
      this._phaseMessage,
      cInfo.phaseFormat.phaseMessage,
    );
  }
  refreshPhaseRemain(cInfo) {
    TimerPageDrawer.updateDomTxt(
      this._phaseRemainTime,
      `${cInfo.clock.phaseRemain.min}:` +
        `${cInfo.clock.phaseRemain.sec}`.padStart(2, "0"),
    );
  }
  refresh(cInfo) {
    this.refreshPhaseFormat(cInfo);
    this.refreshStartDate(cInfo);
    this.refreshCurrentTime(cInfo);
    this.refreshNewPhaseTill(cInfo);
    this.refreshHeader(cInfo);
    this.refreshPhaseRemain(cInfo);
  }
}

/** audio file list
 * name field for label, src for audio path
 */
const defaultAudioList = [
  {
    name: "prior5min",
    src: "/sounds/01_5min.mp3",
  },
  {
    name: "move1st",
    src: "/sounds/02_first-move.mp3",
  },
  {
    name: "assignmentRead",
    src: "/sounds/03_read.mp3",
  },
  {
    name: "assignmentStart",
    src: "/sounds/04_skill.mp3",
  },
  {
    name: "assignmentLeft1min",
    src: "/sounds/05_1min.mp3",
  },
  {
    name: "assignmentFinishMove",
    src: "/sounds/06_move-next.mp3",
  },
];

/** audio announcement
 * control audio announcement
 */
class AudioAnnounce {
  static playback(afobj, aobj) {
    if (aobj.isActive && afobj.af != aobj.lastPlaybacked) {
      aobj.lastPlaybacked = afobj.af;
      try {
        afobj.af.play();
        console.log(
          `${new Date(Date.now()).toLocaleString()} -- ` +
            `play: ${afobj.name}`,
        );
      } catch (_e) {
        console.log(
          `${new Date(Date.now()).toLocaleString()} -- ` +
            `failed to play: ${afobj.name} [${_e}]`,
        );
      }
      return true;
    }
  }

  _init(afList = defaultAudioList) {
    afList.forEach((f) => {
      this[f.name] = {
        name: f.name,
        src: f.src,
        af: new Audio(),
        cue: () => {
          AudioAnnounce.playback(this[f.name], this);
        },
      };
    });
  }
  constructor(afList = defaultAudioList) {
    this._afList = afList;
    // init announcement
    try {
      this._init(afList);
    } catch (_e) {
      this._init(defaultAudioList);
    }
    this.isActive = false;
    this.lastPlaybacked = null;
    // console.log("instance of AudioAnnounce created");
  }
  enable() {
    // load audio files on construc
    this._afList.forEach((f) => {
      this[f.name].af.src = this[f.name].src;
    });
    this.isActive = true;
    console.log("audio files loaded");
  }
  disable() {
    this._afList.forEach((f) => {
      this[f.name].af = new Audio();
    });
    this.isActive = false;
    console.log("audio files unloaded");
  }
}

/** event timer
 * manage event process
 * generate neccesary content
 */
export class EventTimer {
  constructor(
    clockObj = new LocalClock(),
    pageFormats = oscePageFormats,
    audioList = defaultAudioList,
  ) {
    this.clock = clockObj;
    this.pageFormats = pageFormats;
    this.announcer = new AudioAnnounce(audioList);
    this.pageDrawer = new TimerPageDrawer();
    this.currentPageFormat = new PageFormat();
    // console.log("instance of EventTimer created");
  }
  set eventStartDateTime(dateObj) {
    this.clock.eventStartDateTime = dateObj;
  }
  get eventStartDateTime() {
    return this.clock.eventStartDateTime;
  }
  set currentDateTime(dateObj) {
    this.clock.currentDateTime = dateObj;
  }
  get currentDateTime() {
    return this.clock.currentDateTime;
  }
  get isNotEventStarted() {
    return this.clock.currentState.phase.name === "beforeEvent";
  }
  get isMovingPhase() {
    return this.clock.currentState.phase.name === "moving";
  }
  get isAssignmentReading() {
    return this.clock.currentState.phase.name === "assignmentRead";
  }
  get isAssignmentRunning() {
    return this.clock.currentState.phase.name === "assignmentRun";
  }
  get isAudioActive() {
    return this.announcer.isActive;
  }
  enableAudio() {
    this.announcer.enable();
  }
  disableAudio() {
    this.announcer.disable();
  }
  pageRefresh() {
    this.pageDrawer.refresh({
      clock: this.clock,
      phaseFormat: this.currentPageFormat,
    });
  }
  run(currentd) {
    this.currentDateTime = currentd;

    /**
     * modify page format and draw content
     */
    switch (true) {
      case this.isNotEventStarted: {
        if (
          this.clock.phaseRemain.isSmallerThan(
            ClockTime.createFromMinSec({ min: 10, sec: 0 }, true),
          )
        ) {
          this.currentPageFormat = this.pageFormats.beforeOSCE;
        } else {
          this.currentPageFormat = this.pageFormats.beforeOSCE10min;
          if (this.clock.phaseRemain.isExactOnMinute(5)) {
            this.announcer.prior5min.cue();
          }
        }

        this.pageRefresh();
        break;
      }

      case this.isMovingPhase: {
        this.currentPageFormat = this.pageFormats.moving;
        if (this.clock.phaseRemain.isExactOnMinute(1)) {
          this.clock.roundTimes === 1
            ? this.announcer.move1st.cue()
            : this.announcer.assignmentFinishMove.cue();
        }

        this.pageRefresh();
        break;
      }

      case this.isAssignmentReading: {
        this.currentPageFormat = this.pageFormats.assignmentRead;
        if (this.clock.phaseRemain.isExactOnMinute(1)) {
          this.announcer.assignmentRead.cue();
        }

        this.pageRefresh();
        break;
      }

      case this.isAssignmentRunning: {
        this.currentPageFormat = this.pageFormats.assignmentRunning;
        if (this.clock.phaseRemain.isExactOnMinute(5)) {
          this.announcer.assignmentStart.cue();
        }
        if (this.clock.phaseRemain.isExactOnMinute(1)) {
          this.announcer.assignmentLeft1min.cue();
        }

        this.pageRefresh();
        break;
      }
    }
    return true; // if not match all cases raise error?
  }
}
