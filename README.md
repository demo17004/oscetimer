# OSCE Timer

## 動作環境

### サーバ

    * OS: macOS or Linux
    * requirement: node.js & npm (v5.7~)

### クライアント

    * webブラウザ
      * Chrome
      * Firefox
      * Safari (音声再生は非サポート)

## インストール

`npm ci --production`

## サーバ設定

方法は下記 3 通り

- `npm run appconf`
- `appconfig.json`を編集
- `http://server.address:port/admin`より変更(後述)

## 実行

### サーバ起動

`npm start`

## クライアント接続

- ブラウザより`http://server.address:port/`へアクセス
- 左下、青表示テキストをタップもしくはクリックで同期インジケータ表示

## クライアント接続（アナウンスあり）

- `http://server.address:port/a`へアクセス
- 左下、青表示テキストをタップもしくはクリックで同期インジケータ表示
- 中央カウントダウンの数字をダブルタップもしくはダブルクリック後、ダイアログに同意し音声有効化 (音声無効化も同手順)
  右上のテキストが`×`で音声無効、`◯`で有効

## クライアント接続（サーバ制御）

- `http://server.address:port/admin`へアクセス
- 以下が可能
  - 開始日時の入力
  - 開始日時をサーバへ送信（その際、自動的にサーバ側で設定ファイル書き換え）
  - サーバクロック送出の開始、停止
  - `Server says >`以降の文字列でサーバ側のレスポンスを一部確認

#### クライアント側からのサーバ制御について

- 以下の状況で有効
  - `package.json`内、`configure: {"allowHttpControl": true}`が`true`のとき
  - 環境変数で`ADMIN_PAGE='true'`が設定されているとき
  - 以下の条件でループバックアドレス(ipv4 で`127.0.0.1`)からのアクセスのみ許容する(デフォルト)
    - `package.json`内、`configure: {"allowGlobalControl": false}`のとき
    - 環境変数で`ADMIN_REMOTE='false'`のとき
- 環境変数と、`package.json`内の設定では、環境変数の設定値が優先される
