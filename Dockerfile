# oscetimer app image
FROM node:17-alpine AS builder

WORKDIR app
COPY package.json package-lock.json ./
RUN npm ci --production
COPY src ./src

FROM node:17-alpine AS runner
WORKDIR /opt/app
COPY --from=builder /app ./
USER node
EXPOSE 3000
CMD ["npm", "start"]
