/**
 * @jest-environment jsdom
 */
"use strict";
import { describe, expect, it, jest } from "@jest/globals";
import { EventTimer, LocalClock } from "../src/public/timerlib.js";

// helper function
function convertDatePropToDateObj(startDate) {
  return new Date(
    `${startDate.year}-` +
      `${startDate.month}`.padStart(2, "0") +
      "-" +
      `${startDate.date}`.padStart(2, "0") +
      "T" +
      `${startDate.hour}`.padStart(2, "0") +
      ":" +
      `${startDate.minute}`.padStart(2, "0"),
  );
}

// setup lclock
const startDate = convertDatePropToDateObj({
  year: 2022,
  month: 1,
  date: 20,
  hour: 9,
  minute: 0,
});
const currentDate = convertDatePropToDateObj({
  year: 2022,
  month: 1,
  date: 20,
  hour: 13,
  minute: 10,
});
const beforeEvDate = convertDatePropToDateObj({
  year: 2022,
  month: 1,
  date: 19,
  hour: 21,
  minute: 0,
});

describe("test LocalClock class", () => {
  it("returns beforeEvent state, given minus millsec", () => {
    const lClock = new LocalClock();
    const beforeEventTime = { millSec: -3000 }; // dummy
    lClock._totalElapsed = beforeEventTime;
    expect(lClock.currentState.phase.name).toBe("beforeEvent");
  });

  it("check state on just start datetime", () => {
    const lClock = new LocalClock();
    lClock.eventStartDateTime = startDate;
    lClock.currentDateTime = startDate;

    expect(lClock.totalElapsed.millSec).toBe(0);
    expect(lClock.currentState.phase.name).toBe("moving");
    expect(lClock.currentState.phase.remain.millSec).toBe(60_000);
    expect(lClock.currentState.rounds).toBe(1);
  });

  it("check returning val after event", () => {
    const lClock = new LocalClock();
    const elapsedTime = { millSec: currentDate - startDate };
    // 5min passed, assignment running on 3 minutes elapsed
    const expectedState = {
      rounds: 36,
      lapTime: 300000, // 5mins in millsec
      phase: {
        name: "assignmentRun",
        elapsed: 3 * 60 * 1000, // 1min must be ClockTime obj
        remain: 2 * 60 * 1000, //  4min must be ClockTime obj
      },
    };
    const exactState = lClock.phases.updateCurrentState(elapsedTime); // lClock.currentState;
    exactState.lapTime = exactState.lapTime.millSec; // replace to int
    exactState.phase.elapsed = exactState.phase.elapsed.millSec; // replace to int
    exactState.phase.remain = exactState.phase.remain.millSec; // replace to int

    expect(exactState).toStrictEqual(expectedState);
  });

  it("correctly set start date", () => {
    const lClock = new LocalClock();
    lClock.eventStartDateTime = startDate;
    expect(lClock.eventStartDateTime).toBeInstanceOf(Date);
    expect(lClock.eventStartDateTime).toStrictEqual(startDate);
  });

  it("correctly set current date", () => {
    const lClock = new LocalClock();
    lClock.currentDateTime = currentDate;
    expect(lClock.currentDateTime).toBeInstanceOf(Date);
    expect(lClock.currentDateTime).toStrictEqual(currentDate);
  });

  it(".isEventStarted returns true if started", () => {
    const lClock = new LocalClock();
    lClock.eventStartDateTime = startDate;
    lClock.currentDateTime = currentDate;
    expect(lClock.isEventStarted).toBe(true);
    lClock.currentDateTime = beforeEvDate;
    expect(lClock.isEventStarted).toBe(false);
  });

  it(".isSTATE returns bool", () => {
    const lClock = new LocalClock();
    lClock.eventStartDateTime = startDate;
    lClock.currentDateTime = currentDate;
    expect(lClock.isEventStarted).toBe(true);
  });

  it(".timeNextPhaseStarts is Date obj", () => {
    const lClock = new LocalClock();
    lClock.eventStartDateTime = startDate;
    lClock.currentDateTime = currentDate;
    expect(lClock.timeNextPhaseStarts).toBeInstanceOf(Date);
  });

  it(".currentState structure", () => {
    const lClock = new LocalClock();
    lClock.eventStartDateTime = startDate;
    lClock.currentDateTime = currentDate;
    expect(lClock.currentState).toHaveProperty("rounds");
    expect(lClock.currentState).toHaveProperty("lapTime");
    expect(lClock.currentState).toHaveProperty("phase.name");
    expect(lClock.currentState).toHaveProperty("phase.elapsed");
    expect(lClock.currentState).toHaveProperty("phase.remain");
  });

  describe("check state update to 200 rounds", () => {
    /** generator for update state
     * returns date and round number
     */
    function* roundGenerator(from = new Date(), roundTill = 200) {
      let currentRound = 1;
      let cDate = from;
      while (currentRound <= roundTill) {
        yield { date: cDate, round: currentRound };
        cDate = new Date(cDate.valueOf() + 7 * 60 * 1000);
        currentRound += 1;
      }
    }

    it("round number update", () => {
      const lClock = new LocalClock();
      lClock.eventStartDateTime = startDate;
      const roundGen = roundGenerator(startDate, 200);
      for (const current of roundGen) {
        lClock.currentDateTime = current.date;
        expect(lClock.roundTimes).toBe(current.round);
      }
    });

    it("moving phase", () => {
      const lClock = new LocalClock();
      lClock.eventStartDateTime = startDate;
      const roundGenAtStart = roundGenerator(startDate, 200);
      for (const current of roundGenAtStart) {
        lClock.currentDateTime = current.date;
        expect(lClock.currentState.phase.name).toBe("moving");
      }
      const roundGenAtEnd = roundGenerator(
        new Date(startDate.valueOf() + 1 * 60_000 - 1),
        200,
      );
      for (const current of roundGenAtEnd) {
        lClock.currentDateTime = current.date;
        expect(lClock.currentState.phase.name).toBe("moving");
      }
    });

    it("assignment reading phase", () => {
      const lClock = new LocalClock();
      lClock.eventStartDateTime = startDate;
      const roundGenAtStart = roundGenerator(
        new Date(startDate.valueOf() + 1 * 60_000),
        200,
      );
      for (const current of roundGenAtStart) {
        lClock.currentDateTime = current.date;
        expect(lClock.currentState.phase.name).toBe("assignmentRead");
      }
      const roundGenAtEnd = roundGenerator(
        new Date(startDate.valueOf() + 2 * 60_000 - 1),
        200,
      );
      for (const current of roundGenAtEnd) {
        lClock.currentDateTime = current.date;
        expect(lClock.currentState.phase.name).toBe("assignmentRead");
      }
    });

    it("assignment running phase", () => {
      const lClock = new LocalClock();
      lClock.eventStartDateTime = startDate;
      const roundGenAtStart = roundGenerator(
        new Date(startDate.valueOf() + 2 * 60_000),
        200,
      );
      for (const current of roundGenAtStart) {
        lClock.currentDateTime = current.date;
        expect(lClock.currentState.phase.name).toBe("assignmentRun");
      }
      const roundGenAtEnd = roundGenerator(
        new Date(startDate.valueOf() + 7 * 60_000 - 1),
        200,
      );
      for (const current of roundGenAtEnd) {
        lClock.currentDateTime = current.date;
        expect(lClock.currentState.phase.name).toBe("assignmentRun");
      }
    });
  });
});

describe("test EventTimer", () => {
  describe("check page format update", () => {
    // prepare test
    const evTimer = new EventTimer();
    const spyDrawer = jest
      .spyOn(evTimer.pageDrawer, "refresh")
      .mockImplementation(() => {});
    evTimer.eventStartDateTime = startDate;

    it("test format before 15 min prior", () => {
      evTimer.run(
        convertDatePropToDateObj({
          year: 2022,
          month: 1,
          date: 20,
          hour: 8,
          minute: 45,
        }),
      );
      expect(evTimer.currentPageFormat.phaseMessage).toBe("OSCE開始まで");
      expect(evTimer.currentPageFormat.bgcolor).toBe("gray");

      evTimer.run(
        convertDatePropToDateObj({
          year: 2022,
          month: 1,
          date: 20,
          hour: 8,
          minute: 50,
        }) - 1,
      );
      expect(evTimer.currentPageFormat.phaseMessage).toBe("OSCE開始まで");
      expect(evTimer.currentPageFormat.bgcolor).toBe("gray");
      spyDrawer.mockClear();
    });

    it("test format before 10 min prior", () => {
      evTimer.run(
        convertDatePropToDateObj({
          year: 2022,
          month: 1,
          date: 20,
          hour: 8,
          minute: 50,
        }),
      );
      expect(evTimer.currentPageFormat.phaseMessage).toBe("OSCE開始まで");
      expect(evTimer.currentPageFormat.bgcolor).toBe("pink");

      evTimer.run(
        convertDatePropToDateObj({
          year: 2022,
          month: 1,
          date: 20,
          hour: 9,
          minute: 0,
        }) - 1,
      );
      expect(evTimer.currentPageFormat.phaseMessage).toBe("OSCE開始まで");
      expect(evTimer.currentPageFormat.bgcolor).toBe("pink");
      spyDrawer.mockClear();
    });

    it("test format at start (move)", () => {
      evTimer.run(startDate);
      expect(evTimer.currentPageFormat.phaseMessage).toBe("移動 1分間");

      evTimer.run(
        convertDatePropToDateObj({
          year: 2022,
          month: 1,
          date: 20,
          hour: 9,
          minute: 1,
        }) - 1, // minus 1 milli sec
      );
      expect(evTimer.currentPageFormat.phaseMessage).toBe("移動 1分間");
      spyDrawer.mockClear();
    });

    it("test format at assignment run", () => {
      evTimer.run(
        convertDatePropToDateObj({
          year: 2022,
          month: 1,
          date: 20,
          hour: 9,
          minute: 1,
        }),
      );
      expect(evTimer.currentPageFormat.phaseMessage).toBe("課題を読む 1分間");

      evTimer.run(
        convertDatePropToDateObj({
          year: 2022,
          month: 1,
          date: 20,
          hour: 9,
          minute: 2,
        }) - 1, // minus 1 milli sec
      );
      expect(evTimer.currentPageFormat.phaseMessage).toBe("課題を読む 1分間");
      spyDrawer.mockClear();
    });

    it("test format at assignment read", () => {
      evTimer.run(
        convertDatePropToDateObj({
          year: 2022,
          month: 1,
          date: 20,
          hour: 9,
          minute: 3,
        }),
      );
      expect(evTimer.currentPageFormat.phaseMessage).toBe("実技 5分間");

      evTimer.run(
        convertDatePropToDateObj({
          year: 2022,
          month: 1,
          date: 20,
          hour: 9,
          minute: 7,
        }) - 1, // minus 1 milli sec
      );
      expect(evTimer.currentPageFormat.phaseMessage).toBe("実技 5分間");
      spyDrawer.mockClear();
    });
  });

  describe("check audio playback cue", () => {
    // prepare test
    const evTimer = new EventTimer();
    evTimer.eventStartDateTime = startDate;
    const spyDrawer = jest
      .spyOn(evTimer.pageDrawer, "refresh")
      .mockImplementation(() => {});

    it("before event prior 5 min, announcement ", () => {
      const spyAnnounce = jest
        .spyOn(evTimer.announcer.prior5min, "cue")
        .mockImplementation(() => {});

      evTimer.run(
        convertDatePropToDateObj({
          year: 2022,
          month: 1,
          date: 20,
          hour: 8,
          minute: 55,
        }),
      );

      expect(spyAnnounce).toHaveBeenCalledTimes(1);

      spyDrawer.mockClear();
      spyAnnounce.mockRestore();
    });

    it("play announcement move1st ", () => {
      const spyAnnounce = jest
        .spyOn(evTimer.announcer.move1st, "cue")
        .mockImplementation(() => {});

      evTimer.run(startDate);

      expect(spyAnnounce).toHaveBeenCalledTimes(1);

      spyDrawer.mockClear();
      spyAnnounce.mockRestore();
    });

    it("play assignment read announcement ", () => {
      const spyAnnounce = jest
        .spyOn(evTimer.announcer.assignmentRead, "cue")
        .mockImplementation(() => {});

      evTimer.run(
        convertDatePropToDateObj({
          year: 2022,
          month: 1,
          date: 20,
          hour: 9,
          minute: 1,
        }),
      );

      expect(spyAnnounce).toHaveBeenCalledTimes(1);

      spyDrawer.mockClear();
      spyAnnounce.mockRestore();
    });

    it("play assignment start announcement", () => {
      const spyAnnounce = jest
        .spyOn(evTimer.announcer.assignmentStart, "cue")
        .mockImplementation(() => {});

      evTimer.eventStartDateTime = startDate;
      evTimer.run(
        convertDatePropToDateObj({
          year: 2022,
          month: 1,
          date: 20,
          hour: 9,
          minute: 2,
        }),
      );
      expect(spyDrawer).toHaveBeenCalled();
      expect(spyAnnounce).toHaveBeenCalled();

      spyDrawer.mockClear();
      spyAnnounce.mockRestore();
    });

    it("play assignment 1 min left announcement", () => {
      const spyAnnounce = jest
        .spyOn(evTimer.announcer.assignmentLeft1min, "cue")
        .mockImplementation(() => {});

      evTimer.eventStartDateTime = startDate;
      evTimer.run(
        convertDatePropToDateObj({
          year: 2022,
          month: 1,
          date: 20,
          hour: 9,
          minute: 6,
        }),
      );

      expect(spyAnnounce).toHaveBeenCalled();

      spyDrawer.mockClear();
      spyAnnounce.mockRestore();
    });

    it("play assignment finish and move announcement", () => {
      const spyAnnounce = jest
        .spyOn(evTimer.announcer.assignmentFinishMove, "cue")
        .mockImplementation(() => {});

      evTimer.run(
        convertDatePropToDateObj({
          year: 2022,
          month: 1,
          date: 20,
          hour: 9,
          minute: 7,
        }),
      );

      expect(spyAnnounce).toHaveBeenCalled();

      spyDrawer.mockClear();
      spyAnnounce.mockRestore();
    });
  });

  describe("check dom update", () => {
    const initDOM = () => {
      document.body.innerHTML = `
      <span id="roundNum"></span>
      <span id="phaseMessage"></span>
    <div id="phaseRemainTime"></div>
      <span id="currentTimeHour"></span>
      <span id="currentTimeMinute"></span>
      <span id="currentTimeSecond"></span>
      <span id="phaseTillMsg"></span>
      <span id="phaseTillHour"></span>
      <span id="phaseTillMinute"></span>
      <span id="phaseTillSecond"></span>
    <div id="devinfo">
      <span id="startDateFromServer"></span>
      <span id="clockIndicator"></span>
    </div>`;
    };

    it("test phase format update", () => {
      initDOM();
      const evTimer = new EventTimer();
      const initPhase = evTimer.pageFormats.beforeOSCE;
      const targetElem = document.getElementById("phaseTillMsg");

      evTimer.pageDrawer.refreshPhaseFormat({ phaseFormat: initPhase });
      expect(document.body.style.backgroundColor).toBe(initPhase.bgcolor);
      expect(document.body.style.color).toBe(initPhase.color);
      expect(targetElem.textContent).toBe(initPhase.phaseTillMsg);
    });

    it("test start date update", () => {
      initDOM();
      const evDraer = new EventTimer().pageDrawer;
      const targetElem = document.getElementById("startDateFromServer");

      evDraer.refreshStartDate({
        clock: { eventStartDateTime: startDate },
      });
      expect(targetElem.textContent).toBe("20日 9:00");
    });

    it("test current time update", () => {
      initDOM();
      const evDrawer = new EventTimer().pageDrawer;
      const targetElem = {
        hour: document.getElementById("currentTimeHour"),
        min: document.getElementById("currentTimeMinute"),
        sec: document.getElementById("currentTimeSecond"),
      };

      const current = new Date(currentDate.valueOf() + 3_100); // 3 sec passed
      evDrawer.refreshCurrentTime({
        clock: { currentDateTime: current },
      });
      const expectedVal = {
        hour: "13",
        min: "10",
        sec: "03",
      };

      for (const [key, val] of Object.entries(targetElem)) {
        expect(val.textContent).toBe(expectedVal[key]);
      }
    });

    it("test new phase till update", () => {
      initDOM();
      const evDrawer = new EventTimer().pageDrawer;
      const targetElem = {
        hour: document.getElementById("phaseTillHour"),
        min: document.getElementById("phaseTillMinute"),
        sec: document.getElementById("phaseTillSecond"),
      };

      const next = new Date(currentDate.valueOf() + 60_000); // 1 min later
      evDrawer.refreshNewPhaseTill({
        clock: { timeNextPhaseStarts: next },
      });
      const expectedVal = {
        hour: "13",
        min: "11",
        sec: "00",
      };

      for (const [key, val] of Object.entries(targetElem)) {
        expect(val.textContent).toBe(expectedVal[key]);
      }
    });

    it("test header update", () => {
      const evDrawer = new EventTimer().pageDrawer;
      const roundNum = document.getElementById("roundNum");
      const phaseMessage = document.getElementById("phaseMessage");
      // test after start
      initDOM();
      evDrawer.refreshHeader({
        clock: { isEventStarted: true, roundTimes: 3 },
        phaseFormat: { phaseMessage: "test phase" },
      });

      expect(roundNum.textContent).toBe("03");
      expect(phaseMessage.textContent).toBe("test phase");

      // test before event
      initDOM();
      evDrawer.refreshHeader({
        clock: { isEventStarted: false, roundTimes: "" },
        phaseFormat: { phaseMessage: "test phase before ev" },
      });

      expect(roundNum.textContent).toBe("");
      expect(phaseMessage.textContent).toBe("test phase before ev");
    });

    it("test phase remain update", () => {
      initDOM();
      const evDrawer = new EventTimer().pageDrawer;
      const phaseRem = document.getElementById("phaseRemainTime");

      evDrawer.refreshPhaseRemain({
        clock: { phaseRemain: { min: 3, sec: 9 } },
      });
      expect(phaseRem.textContent).toBe("3:09");
    });
  });
});
